#!/bin/sh
#
# Purpose {{{
# This script will …
#   1. …
# …
#
# !!DATE!!
# }}}
# Flags {{{
## Exit on error {{{
set -o errexit
## }}}
## Exit on unset var {{{
### Use "${VARNAME-}" to test a var that may not have been set
set -o nounset
## }}}
## Pipeline command is treated as failed {{{
### Not available in POSIX sh − https://github.com/koalaman/shellcheck/wiki/SC3040
#set -o pipefail
## }}}
## Help with debugging {{{
### Call the script by prefixing it with "TRACE=1 ./script.sh"
if [ "${TRACE-0}" -eq 1 ]; then set -o xtrace; fi
## }}}
# }}}
# Vars {{{
PROGNAME=$(basename "${0}"); readonly PROGNAME
PROGDIR=$(readlink --canonicalize-missing $(dirname "${0}")); readonly PROGDIR
ARGS="${*}"; readonly ARGS
readonly NBARGS="${#}"
[ -z "${DEBUG-}" ] && DEBUG=1
## Export DEBUG for sub-script
export DEBUG

## Default values for some vars
#readonly MY_VAR_XY_DEFAULT="666"

## Colors
readonly PURPLE='\033[1;35m'
readonly RED='\033[0;31m'
readonly RESET='\033[0m'
readonly COLOR_DEBUG="${PURPLE}"
# }}}
usage() {                                                       # {{{

	cat <<- HELP
usage: $PROGNAME [-d|-h]

Try to give a description…

EXAMPLES :
    - Apply my script to…
        ${PROGNAME}

OPTIONS :
    -d,--debug
        Enable debug messages.

    -h,--help
        Print this help message.
HELP
}
# }}}
debug_message() {                                               # {{{

	local_debug_message="${1}"

	## Print message if DEBUG is enable (=0)
	[ "${DEBUG}" -eq "0" ] && printf '\e[1;35m%-6b\e[m\n' "DEBUG − ${PROGNAME} : ${local_debug_message}"

	unset local_debug_message

	return 0
}
# }}}
error_message() {                                               # {{{

	local_error_message="${1}"
	local_error_code="${2}"

	## Print message
	printf '%b\n' "ERROR − ${PROGNAME} : ${RED}${local_error_message}${RESET}" >&2

	unset local_error_message

	exit "${local_error_code:=66}"
}
# }}}
define_vars() {                                                 # {{{

	## If my_var_xy wasn't defined (argument) {{{
	#if [ -z "${my_var_xy-}" ]; then
		### Use default value
		#readonly my_var_xy="${MY_VAR_XY_DEFAULT}"
	#fi
	## }}}

	true; ## Remove me
}
# }}}

is_command_available() {                                        # {{{

	local_command_available_cmd="${1}"
	debug_prefix="${2:-}"

	## Return False by default
	return_command_available="1"

	if [ "$(command -v ${local_command_available_cmd})" ]; then
		debug_message "${debug_prefix}is_command_available − \
${RED}${local_command_available_cmd}${COLOR_DEBUG} seems present on this host."
		return_command_available="0"
	else
		debug_message "${debug_prefix}is_command_available − \
${RED}${local_command_available_cmd}${COLOR_DEBUG} is not available on this host."
		return_command_available="1"
	fi

	unset local_command_available_cmd
	unset debug_prefix

	return "${return_command_available}"
}
# }}}

main() {                                                        # {{{

	debug_message "--- MAIN BEGIN"

	## If a command is missing {{{
	### Exit with error message
	is_command_available "test_me" "| " \
		|| error_message "No test_me command available. Please install test_me package with your package manager." 01
	## }}}

	## Define all vars
	define_vars
	debug_message "| Define vars"

	debug_message "--- MAIN END"
}
# }}}

# Manage arguments                                                # {{{
# This code can't be in a function due to argument management

if [ ! "${NBARGS}" -eq "0" ]; then

	manage_arg="0"

	## If the first argument ask for help (h|help|-h|-help|-*h|-*help) {{{
	if printf -- '%s' "${1-}" | grep --quiet --extended-regexp -- "^-*h(elp)?$"; then
		usage
		exit 0
	fi
	## }}}

	## If the first argument is not an option
	if ! printf -- '%s' "${1}" | grep --quiet --extended-regexp -- "^-+";
	then
		## Print help message and exit
		printf '%b\n' "${RED}Invalid option: ${1}${RESET}"
		printf '%b\n' "---"
		usage

		exit 1
	fi

	# Parse all options (start with a "-") one by one
	while printf -- '%s' "${1-}" | grep --quiet --extended-regexp -- "^-+"; do

	case "${1}" in
		-d|--debug )                          ## debug
			DEBUG=0
			debug_message "--- Manage argument BEGIN"
			;;
		#-v|--var )                            ## Define var with given arg
			### Move to the next argument
			#shift
			### Define var
			#readonly my_var_xy="${1}"
			#;;
		* )                                   ## unknow option
			printf '%b\n' "${RED}Invalid option: ${1}${RESET}"
			printf '%b\n' "---"
			usage
			exit 1
			;;
	esac

	debug_message "| ${RED}${1}${COLOR_DEBUG} option managed."

	## Move to the next argument
	shift
	manage_arg=$((manage_arg+1))

	done

	debug_message "| ${RED}${manage_arg}${COLOR_DEBUG} argument(s) successfully managed."
else
	debug_message "| No arguments/options to manage."
fi

	debug_message "--- Manage argument END"
# }}}

main

exit 255
