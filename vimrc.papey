" --------------------------------------------------------------------------------------------------
" |                             Fichier de configuration de vim                                    |
" |                                  Emplacement : ~/.vimrc                                        |
" | Auteur               : Gardouille Feat Papey                                                   |
" | Derniers changements : Refonte totale du fihier                                                |
" | Date                 : 2014/12/02                                                              |
" | Version              : 0.1                                                                     |
" --------------------------------------------------------------------------------------------------

""""""""""""""""""""""""""""""""""
" Options de bases
""""""""""""""""""""""""""""""""""
" Options diverses en vrac
let g:hybrid_use_Xresources = 1               " le colorscheme utilise les couleurs du .Xressources
colorscheme hybrid                            " colorscheme
syntax enable                                 " activation de la coloration syntaxique
set number                                    " numérotation des lignes
set autoindent                                " indentation automatique avancée
set smartindent                               " indentation plus intelligente
set backspace=indent,eol,start                " autorisation du retour arrière
set bs=2                                      " redéfinition du backspace
set history=50                                " fixe l'historique à 50 commandes maxi
set ruler                                     " affiche la position courante au sein du fichier
set showcmd                                   " affiche la commande en cours
set shiftwidth=4                              " taille des tabulations (nb d'espace)
set softtabstop=4                             " taille des tabulations mixtes (tabulations et espaces)
set tabstop=4                                 " taille des tabulations à l'affichage (nb d'espace)
set expandtab                                 " transforme les tabulations en espaces
set showmatch                                 " vérification présence (, [ ou { à la frappe de ), ] ou }
filetype plugin indent on                     " détection automatique du type de fichier
autocmd FileType text setlocal textwidth=80   " les fichiers de type .txt sont limités à 80 caractères par ligne
autocmd FileType tex setlocal textwidth=80    " les fichiers de type .tex sont limités à 80 caractères par ligne
set fileformats=unix,mac,dos                  " gestion des retours chariot en fonction du type de fichier
set hlsearch                                  " surligne les résultats de la recherche
" set nohls                                    " ne pas surligner les résultats de la recherche
set incsearch                                 " recherche en même temps que la saisie
set ignorecase                                " ne pas prendre en compte la casse pour les recherches
"set noic                                      " Prendre en compte la casse pour les recherches
"set smartcase                                 " recherche respectueuse de la case quand une majuscule est saisie
set cursorline                                " met en avant la ligne courante
"set cursorcolumn                             " met en avant la colonne courante
set so=2                                      " Place le curseur sur la 2ème ligne lors de mouvements verticaux
set pt=<F11>                                  " évite la double indentation lors de c/c
set t_Co=256                                  " pour tmux 256 colors
set cpoptions+=$                              " ajoute un $ pour indiquer la fin d'un remplacement (ex avec un "ce")
set virtualedit=all                           " Permet de se déplacer la ou il n'y a pas de caractères
set colorcolumn=81                            " Coloration bar caractère 80

" Couleur des éléments
hi StatusLine ctermfg=black ctermbg=green
hi TabLineFill ctermfg=black ctermbg=grey
hi TabLine ctermfg=black ctermbg=red
hi TabLineSel ctermfg=green ctermbg=black

" White space characters
set list
set listchars=eol:¬,trail:\

" Placer les fichiers .swp dans un autre répertoire
if !filewritable ($HOME."/.vim/tmp") " Si le répertoire n'existe pas
     call mkdir($HOME."/.vim/tmp", "p") " Création du répertoire temporaire
endif
set directory=$HOME/.vim/tmp

""""""""""""""""""""""""""""""""""""""""""""""""
" Touche MapLeader :
""""""""""""""""""""""""""""""""""""""""""""""""
" Activation de la touche mapleader qui permet de faire des combinaisons
" supplémentaires
let mapleader = ","
let g:mapleader = ","

" Sauvegarde rapide
nmap <leader>w :w!<cr>
nmap <leader>q :wq<cr>

" Édition rapide de vimrc
map <leader>e :e! ~/.vimrc<cr>
" Recharge le vimrc
map <leader>< :source ~/.vimrc<cr>

" Navigation dans les buffers
" Détails sur les buffers: http://vim-fr.org/index.php/Buffer
map <leader>t :bp<cr>
map <leader>s :bn<cr>

"Navigation splits
map <leader>j <C-w>j
map <leader>h <C-w>h
map <leader>k <C-w>k
map <leader>l <C-w>l


""""""""""""""""""""""""""""""""""""""""""""""""
" Exercice mon gars
""""""""""""""""""""""""""""""""""""""""""""""""
" UN PEU D'EXERCICE BORDEL
" H pour <-
" L pour ->
" J pour flèche bas
" K pour flèche haut
""""""""""""""""""""""""""""""""""""""""""""""""
"Désactiver les flèches
" Mode commande
map <right> <esc>
map <left> <esc>
map <up> <esc>
map <down> <esc>
" Mode insertion
imap <right> <esc>
imap <left> <esc>
imap <up> <esc>
imap <down> <esc>


""""""""""""""""""""""""""""""""""""""""""""""""
" Mapping :
""""""""""""""""""""""""""""""""""""""""""""""""
"Désactive le surlignage des résultats d'une recherche en utilisant CTRL-n
nnoremap <silent> <C-N> :noh<CR>
"Ajoute une ligne avant le curseur sans passer en mode insertion
nnoremap <Space><Enter> o<ESC>
"Ajoute une ligne après le curseur sans passer en mode insertion
map <Enter> O<ESC>

"Permet la compilation d'un document latex grace a F6
inoremap <leader>i <Esc>:w<cr>:!pdflatex %<cr>i
"Raccourcis pour un make plus rapide
inoremap <leader>m <Esc>:w<cr>:make<cr>
"avance d'un caratere en mode insert (vim-autoclose)
inoremap <leader>n <Esc>l i

" Remap de echap sur jj
inoremap jj <esc>

" Se déplacer dans les onglets
map <tab> gt


""""""""""""""""""""""""""""""""""""""""""""""""
" Positionner le curseur à l'emplacement de la
"dernière édition
""""""""""""""""""""""""""""""""""""""""""""""""
set viminfo='10,\"100,:20,%,n~/.viminfo
au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif


"""""""""""""""""""""""""""""""""""""""""""""""""""
" Création et centralisation des backups et undo persistant
"""""""""""""""""""""""""""""""""""""""""""""""""""
if !filewritable ($HOME."/.vim/backup") " Si le répertoire n'existe pas
  call mkdir($HOME."/.vim/backup", "p") " Création du répertoire de sauvegarde
endif
" On définit le répertoire de sauvegarde
set backupdir=$HOME/.vim/backup

" On active le comportement précédemment décrit
set backup

" Undo persistant
if !filewritable ($HOME."/.vim/undodir") " Si le répertoire n'existe pas
  call mkdir($HOME."/.vim/undodir", "p") " Création du répertoire de sauvegarde
endif
set undodir=~/.vim/undodir " répertoire où seront stockés les modifications
set undofile               " activation du undo persistant
set undolevels=100         " nombre maximum de changements sauvegardés
set undoreload=100         " nombre maximum de lignes sauvegardées


""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins :
""""""""""""""""""""""""""""""""""""""""""""""""
" ----- Vundle
set nocompatible              " be iMproved
filetype off                  " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required!
Bundle 'gmarik/vundle'

" My bundles here:
"
" original repos on GitHub
Bundle 'tpope/vim-fugitive'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'scrooloose/nerdtree'
Bundle 'scrooloose/nerdcommenter'
Bundle 'bling/vim-airline'
Bundle 'mbbill/undotree'
Bundle 'Townk/vim-autoclose'
Bundle 'edkolev/tmuxline.vim'
Bundle 'Shougo/neocomplete.vim'
Bundle 'Shougo/neosnippet.vim'
Bundle 'honza/vim-snippets'
Bundle 'SirVer/ultisnips'
Bundle 'majutsushi/tagbar'
Bundle 'kien/ctrlp.vim'
Bundle 'jacquesbh/vim-showmarks'
" vim-scripts repos
" non-GtHub repos
" Git repos on your local machine (i.e. when working on your own plugin)
" ...

filetype plugin indent on     " required!
"
" Brief help
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install (update) bundles
" :BundleSearch(!) foo - search (or refresh cache first) for foo
" :BundleClean(!)      - confirm (or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle commands are not allowed.

""""""""""""""""""""""""""""""""""""""""""""""""
" Configuration des plugins :
""""""""""""""""""""""""""""""""""""""""""""""""

" Nerdtree
map <leader>z :NERDTreeToggle<CR> " Mapping pour l'activer/désactiver
let NERDTreeWinPos='right' " Positionné à droite

" UndoTree
map <leader>q :UndotreeToggle<CR> " Mapping pour l'activer/désactiver

" Vim airline
set laststatus=2
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='lucius'

" Neocompcache
let g:neocomplete#enable_at_startup = 1

imap kk     <Plug>(neosnippet_expand_or_jump)

" SuperTab like snippets behavior.
imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: pumvisible() ? "\<C-n>" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: "\<TAB>"

" For snippet_complete marker.
if has('conceal')
  set conceallevel=2 concealcursor=i
endif

" Enable snipMate compatibility feature.
let g:neosnippet#enable_snipmate_compatibility = 1

" Tell Neosnippet about the other snippets
let g:neosnippet#snippets_directory='~/.vim/bundle/vim-snippets'

" Show fucking show marks
noremap ' :ShowMarksOnce<cr> '

