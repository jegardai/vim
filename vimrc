"
" ----------------------------------------------------------------------
" |                             Fichiers de configuration de vim       |
" |                                  Emplacement : ~/.vim/vimrc        |
" | Auteur               : Gardouille                                  |
" | Version              : 1.12                                        |
" |                                                                    |
" | ----------------------- Modification à apporter -------------------|
" | Modifications à apporter:                                          |
" |   - voir le pliage/dépliage (za, zm, zr, ...)                      |
" |   - voir l'option d'encodage (encoding, ...)                       |
" |   - remapper l'incrémentation sur un chiffre avec CTRL-A           |
" |   - permettre une incrémentation sur les lettres, hexa, ...        |
" |   en plus des chiffres                                             |
" -------------------------------------------------------------------- |
" |                                                                    |
" | --------------------------------------- Liens utiles ------------- |
" | Liens utiles:                                                      |
" | http://www.debian-fr.org/vim-t8605.html                            |
" | http://www.swaroopch.com/notes/Vim_fr:Table_des_Mati%C3%A8res      |
" | http://www.rayninfo.co.uk/vimtips.html                             |
" | http://www.vim.org/tips/index.php                                  |
" | http://linuxfr.org/news/vim-f%C3%AAte-son-20e%C2%A0anniversaire    |
" | http://vimdoc.sourceforge.net/htmldoc/map.html#mapmode-n           |
" ----------------------------------------------------------------------

""""""""""""""""""""""""""""""""""""""""""""""""
" Diverses options
""""""""""""""""""""""""""""""""""""""""""""""""
" For all default colorschemes, see `ls -l /usr/share/vim/vim*/colors/`
" For extra colorschemes, see `ls -l ~/.vim/colors`
set t_Co=256                                  " for tmux 256 colors
if filereadable ($HOME."/.vim/colors/one.vim")
	colorscheme one
	set background=light
else
	if filereadable ($HOME."/.vim/colors/codedark.vim")
		colorscheme codedark                        " codedark colorscheme if available
		let g:HLColorScheme = g:colors_name
		set background=dark
	else
		colorscheme default                         " default colorscheme
	endif
endif
syntax on                                     " activation de la coloration syntaxique
set number                                    " numérotation des lignes
hi LineNr ctermbg=none ctermfg=2
"set autoindent                               " indentation automatique avancée
"set smartindent                              " indentation plus intelligente
"set backspace=indent,eol,start                " autorisation du retour arrière
set bs=2                                      " redéfinition du backspace
set history=50                                " fixe l'historique à 50 commandes maxi
set ruler                                     " affiche la position courante au sein du fichier
set showcmd                                   " affiche la commande en cours
set shiftwidth=2                              " taille des tabulations (nb d'espace)
set softtabstop=2                             " taille des tabulations mixtes (tabulations et espaces)
set tabstop=2                                 " taille des tabulations à l'affichage (nb d'espace)
"set expandtab                                 " transforme les tabulations en espaces
set showmatch                                 " vérification présence (, [ ou { à la frappe de ), ] ou }
filetype plugin indent on                     " détection automatique du type de fichier
autocmd FileType text setlocal textwidth=80   " les fichiers de type .txt sont limités à 80 caractères par ligne
autocmd FileType tex setlocal textwidth=80    " les fichiers de type .tex sont limités à 80 caractères par ligne
set fileformats=unix,mac,dos                  " gestion des retours chariot en fonction du type de fichier
set hlsearch                                  " surligne les résultats de la recherche
" set nohls                                    " ne pas surligner les résultats de la recherche
set incsearch                                 " recherche en même temps que la saisie
set ignorecase                                " ne pas prendre en compte la casse pour les recherches
"set noic                                      " Prendre en compte la casse pour les recherches
set smartcase                                 " recherche respectueuse de la case quand une majuscule est saisie
set cursorline                                " met en avant la ligne courante
"set cursorcolumn                             " met en avant la colonne courante
set so=2                                      " Place le curseur sur la 2ème ligne lors de mouvements verticaux
set cpoptions+=$                              " ajoute un $ pour indiquer la fin d'un remplacement
set title                                     " Modifier le titre du terminal (ne semble pas fonctionner avec screen)
set autochdir                                 " Modifie le répertoire courant pour vim en fonction du fichier ouvert
set wildignore=*.swp,*.bak                    " Liste des fichiers ignorés lors de l'auto-complétion
set wildmode=longest:full                     "
set wildmenu                                  " Use menu for command line completion
set virtualedit=all                           " Permet de se déplacer la ou il n'y a pas de caractères
set formatoptions+=awt
set colorcolumn=81                            " By default, color the column after 80 characters
set wrapmargin=0
set foldmethod=marker                         " Use folding with marker (default: {{{ or }}})

" Encodage par défaut des buffers et des fichiers
set encoding=utf-8
set fileencoding=utf-8

" Specific to git commit (COMMIT_EDITMSG) {{{
autocmd Filetype gitcommit setlocal textwidth=72
autocmd Filetype gitcommit setlocal colorcolumn=51,72
" }}}

" Path pour la recherche de fichier avec :find, :sfind et :tabfind
" :find ouvrira à la place du fichier en cours le fichier trouvé
" :sfind splittera l'écran
" :tabfind ouvrira le fichier dans un nouvel onglet
set path=.,/usr/include,/usr/local/include

" Couleur des éléments
"hi StatusLine ctermfg=black ctermbg=green
"hi TabLineFill ctermfg=black ctermbg=grey
"hi TabLine ctermfg=black ctermbg=red
"hi TabLineSel ctermfg=green ctermbg=black


""""""""""""""""""""""""""""""""""""""""""""""""
" Touche MapLeader :
""""""""""""""""""""""""""""""""""""""""""""""""
" Activation de la touche mapleader qui permet de faire des combinaisons
" supplémentaires
let mapleader = ","
let g:mapleader = ","

" Sauvegarde rapide
nmap <leader>w :w<cr>
imap <leader>w <esc>:w<cr>
nmap <leader>q :wq<cr>
imap <leader>q <esc>:wq<cr>
map <leader>a <Esc>:wa<CR>
" Sauvegarder et exécuter le fichier courant
nmap <leader>x :w<cr>:!./"%"<cr>

" Édition rapide de vimrc avec <leader>+e
map <leader>e :e! ~/.vim/vimrc<cr>

" Splits {{{
" Open new split on the bottom part
set splitbelow
" Open new split on the right part
set splitright
" }}}

" FZF function to list all files on the system {{{
function! FzfFiles()
  let tmp = tempname()
  "execute '!ls | fzf >'.tmp
  " Run fzf with update DEFAULT_COMMAND
  " Change the prompt and preview with batcat
  " Store filename in tmp
  execute '!FZF_DEFAULT_COMMAND="fd -uu --type f --search-path /" fzf
\    --prompt="vi> " --preview "bat --color=always --line-range 0:50 {}"
\    >'.tmp
  let g:fzffilename = readfile(tmp)[0]
  silent execute '!rm '.tmp
  "execute 'split '.fname
endfunction
"nnoremap <Leader><Space> :Sopen
nnoremap <Leader><Space> :silent call FzfFiles()<CR> :execute 'split '.fzffilename<CR>
" }}}

" With fzf#run plugin WAY TO SLOW {{{
"
"nnoremap <silent> <Leader><Space> :call fzf#run({
"\   'down': '40%',
"\   'sink': 'split' })<CR>
"nnoremap <silent> <Leader><Enter> :call fzf#run({
"\   'down': '40%',
"\   'sink': 'vsplit' })<CR>

"command! Sudosave :execute ':silent w !sudo tee % > /dev/null' | :edit!
"command! Sopen :execute ':!fd -uu --search-path / --type file | fzf
"\   --prompt="vi> " --preview "bat --color=always --line-range 0:50 {}"
"\   --no-multi' | :split
"command! Sopen :execute ':!FZF_DEFAULT_COMMAND="fd -uu --type f --search-path /"
"\  fzf --prompt="vi> " --preview "bat --color=always --line-range 0:50 {}"
"\   --no-multi' | split
"nnoremap <Leader><Space> :Sopen

"nnoremap <silent> <Leader><Space> :call fzf#run({
"\   'down': '25%',
"\   'sink':  'vertical botright split' })<CR>
" }}}

""" Buffers
" Gestion des splits
map <C-w><Space> :split<Space>
map <C-w><Return> :vsplit<Space>
"map <leader><Space> :split<Space>



" Navigation
" Détails sur les buffers: http://vim-fr.org/index.php/Buffer
"map <leader>p :bp<cr>
"map <leader>s :bn<cr>
map <leader>b <Esc>:b<Space>

"Navigation splits
map <leader>j <C-w>j
map <leader>h <C-w>h
map <leader>k <C-w>k
map <leader>l <C-w>l

" Save with sudo permissions
command! Sudosave :execute ':silent w !sudo tee % > /dev/null' | :edit!
nmap <leader>s :Sudosave<cr>
imap <leader>s <esc>:Sudosave<cr>

""""""""""""""""""""""""""""""""""""""""""""""""
" UN PEU D'EXERCICE
" H pour <-
" L pour ->
" J pour flèche bas
" K pour flèche haut
""""""""""""""""""""""""""""""""""""""""""""""""
"Désactiver les flèches
" Mode commande
nmap <right> <esc>
nmap <left> <esc>
nmap <up> <esc>
nmap <down> <esc>
" Mode insertion:
imap <right> <esc>
imap <left> <esc>
imap <up> <esc>
imap <down> <esc>

""""""""""""""""""""""""""""""""""""""""""""""""
" Mapping - Raccourcis clavier
""""""""""""""""""""""""""""""""""""""""""""""""
"Désactive le surlignage des résultats d'une recherche en utilisant CTRL-n
nnoremap <silent> <C-N> :set hlsearch!<cr>:set hlsearch?<cr>
"Ajoute une ligne avant le curseur sans passer en mode insertion
nnoremap <Space><Enter> o<ESC>

"Ajoute une ligne après le curseur sans passer en mode insertion
map <Enter> O<ESC>

" Activer/désactiver la surbrillance des recherches avec F2
noremap <F2> :set hlsearch!<cr>:set hlsearch?<cr>
" Activer/désactiver la correction avec F3
"noremap <F3> :set spell!<cr>:set spell?<cr>

" Esc mapping in insert mode
imap jj <ESC>
imap àà <ESC>
imap <C-j> <ESC>

""""""""""""""""""""""""""""""""""""""""""""""""
" Vim-Scripts :
""""""""""""""""""""""""""""""""""""""""""""""""
"##########################
" Vundle :
"##########################
" https://github.com/VundleVim/Vundle.vim
" IF not available, use:
" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/vundle
set nocompatible               " be iMproved

""" User interface {{{
    """ Custom highlighting, where NONE uses terminal background {{{
function! CustomHighlighting()
  highlight Normal      ctermbg=NONE
  highlight NonText     ctermbg=NONE
  highlight LineNr      ctermbg=NONE
  highlight SignColumn  ctermbg=NONE
  highlight SignColumn  guibg=#151515
  highlight CursorLine  ctermbg=254
endfunction

call CustomHighlighting()
    """ }}}
""" }}}

filetype plugin indent on     " required!


"##########################
" Pathogen :
"##########################
" http://github.com/tpope/vim-pathogen
"Plug "tpope/vim-pathogen"

"execute pathogen#infect()

"##########################
" Nerd_tree :
"##########################
" https://github.com/scrooloose/nerdtree
" Nerd_tree - Permet d'afficher une arborescence du répertoire courant.
"Plug "scrooloose/nerdtree"

" Raccourcis de la commande avec F10
map <F10> :NERDTreeToggle<cr>
" Et <leader>n
nmap <leader>n :NERDTreeToggle<cr>
" Placer le volet à droite
let NERDTreeWinPos='right'

"##########################
" Nerd_commenter :
"##########################
" https://github.com/scrooloose/nerdcommenter
" Mettre en commentaire une ligne
" Également disponible dans le paquet vim-scripts
"Plug "scrooloose/nerdcommenter"

" Commentaire basique, mais permet également de décommenter (Attention, il y a
" un espace avec le C!)
" Commenter/décommenter une ligne
map ,, <leader>c<space>
"map <F7> <Leader>c
imap  <F7> <esc><F7>
" Commentaire plus sexy, mais impossible à décommenter
map <F8> <Leader>cs
" Note, utilisé le mode visuel ligne SHIFT+V ou le mode visuel bloc CTRL+V

"##########################
" Supertab-continued :
"##########################
" http://github.com/ervandew/supertab
" SuperTab offre une autocomplétion amélioré.
" http://www.vim.org/scripts/script.php?script_id=182
"Plug "ervandew/supertab"

" CTRL-N en mode insertion pour rechercher le suivant
" CTRL-X - CTRL-L: rechercher une ligne complète à partir du début
" CTRL-X - CTRL-K: rechercher dans le dictionnaire
" Permet notamment de compléter avec des noms de variables déjà définies.
" Pas de configuration spécifiques
"imap <leader>d 
"imap <leader>l 

"##########################
" Syntastic :
"##########################
" https://github.com/scrooloose/syntastic
"Plug "scrooloose/syntastic"

" Won't work ...
let g:syntastic_mode_map = { 'mode': 'active',
			\ 'active_filetypes': ['markdown', 'yaml', 'php', 'ruby', 'sh', 'zsh'] }
let g:syntastic_yaml_checkers = ['yamllint']

"##########################
" Neocomplcache :
"##########################
" http://github.com/Shougo/neocomplcache
"Plug "Shougo/neocomplcache"

" Use neocomplcache.
"let g:neocomplcache_enable_at_startup = 1
"" Set minimum syntax keyword length.
"let g:neocomplcache_min_syntax_length = 3
"let g:neocomplcache_lock_buffer_name_pattern = '\*ku\*'


"##########################
" Airline :
"##########################
" https://github.com/bling/vim-airline
"Plug 'bling/vim-airline'

" Toujours afficher la barre de statut
"set laststatus=2                                     " always show statusline
"" Smarter tab line
"let g:airline#extensions#tabline#enabled = 1
""let g:airline#extensions#tabline#left_sep = 'ˇ'
""let g:airline#extensions#tabline#left_alt_sep = 'v'
"let g:airline_powerline_fonts = 1
""let g:airline_theme='molokai'
""let g:airline_theme='lucius'
"let g:airline_theme='monochrome'

"##########################
" Tmuxline :
"##########################
" https://github.com/edkolev/tmuxline.vim
"Plug 'edkolev/tmuxline.vim'

"##########################
" Lightline :
"##########################
""" Lightline {{{

set laststatus=2                                     " always show statusline
let g:lightline = {
  \ 'colorscheme': 'jellybeans',
  \ 'active': {
  \     'left': [
  \         ['mode', 'paste'],
  \         ['readonly', 'fugitive'],
  \         ['ctrlpmark', 'bufferline']
  \     ],
  \     'right': [
  \         ['lineinfo'],
  \         ['percent'],
  \         ['fileformat', 'fileencoding', 'filetype', 'syntastic']
  \     ]
  \ },
  \ 'component': {
  \     'paste': '%{&paste?"!":""}'
  \ },
  \ 'component_function': {
  \     'mode'         : 'MyMode',
  \     'fugitive'     : 'MyFugitive',
  \     'readonly'     : 'MyReadonly',
  \     'ctrlpmark'    : 'CtrlPMark',
  \     'bufferline'   : 'MyBufferline',
  \     'fileformat'   : 'MyFileformat',
  \     'fileencoding' : 'MyFileencoding',
  \     'filetype'     : 'MyFiletype'
  \ },
  \ 'component_expand': {
  \     'syntastic': 'SyntasticStatuslineFlag',
  \ },
  \ 'component_type': {
  \     'syntastic': 'middle',
  \ },
  \ 'subseparator': {
  \     'left': 'ᑄ', 'right': 'ᑁ'
  \ }
  \ }

let g:lightline.mode_map = {
  \ 'n'      : ' Normal ',
  \ 'i'      : ' Insert ',
  \ 'R'      : ' Replace ',
  \ 'v'      : ' Visual ',
  \ 'V'      : 'Visual-L',
  \ 'c'      : ' C ',
  \ "\<C-v>" : 'Visual-B',
  \ 's'      : ' Sub ',
  \ 'S'      : 'Sub-L',
  \ "\<C-s>" : 'Sub-B',
  \ '?'      : '      ' }

        function! MyMode()
            let fname = expand('%:t')
            return fname == '__Tagbar__' ? 'Tagbar' :
                    \ fname == 'ControlP' ? 'CtrlP' :
                    \ winwidth('.') > 60 ? lightline#mode() : ''
        endfunction

        function! MyFugitive()
            try
                if expand('%:t') !~? 'Tagbar' && exists('*fugitive#head')
                    let mark = '± '
                    let _ = fugitive#head()
                    return strlen(_) ? mark._ : ''
                endif
            catch
            endtry
            return ''
        endfunction

        function! MyReadonly()
            return &ft !~? 'help' && &readonly ? '≠' : '' " or ⭤
        endfunction

        function! CtrlPMark()
            if expand('%:t') =~ 'ControlP'
                call lightline#link('iR'[g:lightline.ctrlp_regex])
                return lightline#concatenate([g:lightline.ctrlp_prev, g:lightline.ctrlp_item
                    \ , g:lightline.ctrlp_next], 0)
            else
                return ''
            endif
        endfunction

        function! MyBufferline()
            call bufferline#refresh_status()
            let b = g:bufferline_status_info.before
            let c = g:bufferline_status_info.current
            let a = g:bufferline_status_info.after
            let alen = strlen(a)
            let blen = strlen(b)
            let clen = strlen(c)
            let w = winwidth(0) * 4 / 11
            if w < alen+blen+clen
                let whalf = (w - strlen(c)) / 2
                let aa = alen > whalf && blen > whalf ? a[:whalf] : alen + blen < w - clen || alen < whalf ? a : a[:(w - clen - blen)]
                let bb = alen > whalf && blen > whalf ? b[-(whalf):] : alen + blen < w - clen || blen < whalf ? b : b[-(w - clen - alen):]
                return (strlen(bb) < strlen(b) ? '...' : '') . bb . c . aa . (strlen(aa) < strlen(a) ? '...' : '')
            else
                return b . c . a
            endif
        endfunction

        function! MyFileformat()
            return winwidth('.') > 90 ? &fileformat : ''
        endfunction

        function! MyFileencoding()
            return winwidth('.') > 80 ? (strlen(&fenc) ? &fenc : &enc) : ''
        endfunction

        function! MyFiletype()
            return winwidth('.') > 70 ? (strlen(&filetype) ? &filetype : 'noft') : ''
        endfunction

        let g:ctrlp_status_func = {
            \ 'main': 'CtrlPStatusFunc_1',
            \ 'prog': 'CtrlPStatusFunc_2',
            \ }

        function! CtrlPStatusFunc_1(focus, byfname, regex, prev, item, next, marked)
            let g:lightline.ctrlp_regex = a:regex
            let g:lightline.ctrlp_prev = a:prev
            let g:lightline.ctrlp_item = a:item
            let g:lightline.ctrlp_next = a:next
            return lightline#statusline(0)
        endfunction

        function! CtrlPStatusFunc_2(str)
            return lightline#statusline(0)
        endfunction

        let g:tagbar_status_func = 'TagbarStatusFunc'

        function! TagbarStatusFunc(current, sort, fname, ...) abort
            let g:lightline.fname = a:fname
            return lightline#statusline(0)
        endfunction

        function! s:syntastic()
            SyntasticCheck
            call lightline#update()
        endfunction

        augroup AutoSyntastic
            autocmd!
            execute "autocmd FileType " .
                        \join(g:syntastic_mode_map["active_filetypes"], ",") .
                        \" autocmd BufWritePost <buffer> :call s:syntastic()"
        augroup END
    """ }}}
""""""""""""""""""""""""""""""""""""""""""""""""
" Paramétrage de exuberant-ctags
"http://ngressier.developpez.com/articles/vim/vim-plugin-taglist/#LC
"Indispensable pour le bon fonctionnement du plugin Taglist
"""""""""""""""""""""""""""""""""""""""""""""""""
let Tlist_Ctags_Cmd = '/usr/bin/ctags'

"##########################
" Showmark :
"##########################
" https://github.com/jacquesbh/vim-showmarks
"Plug 'jacquesbh/vim-showmarks'
" Show fucking marks
noremap ' :ShowMarksOnce<cr> '

"##########################
" CTRL P :
"##########################
" https://github.com/kien/ctrlp.vim
"Plug 'kien/ctrlp.vim'

"##########################
" undo-tree :
"##########################
" https://github.com/mbbill/undotree
"Plug 'mbbill/undotree'
" UndoTree
map <leader>u :UndotreeToggle<CR> " Mapping pour l'activer/désactiver

"##########################
" auto-close :
"##########################
" Fermeture automatique des (, [, {, ...
" https://github.com/Townk/vim-autoclose
"Plug 'Townk/vim-autoclose'
" Disable auto-close
imap <silent> <leader>c <Esc>:AutoCloseToggle<CR>
"avance d'un caratere en mode insert (vim-autoclose -> plus besoin de fermer les (, [, {, ...)
imap <leader>n <esc>l i

"##########################
" tagbar:
"##########################
" https://github.com/majutsushi/tagbar
"Plug 'majutsushi/tagbar'
" Open Tagbar:
nmap <leader>t :TagbarToggle<CR>

"##########################
" fugitive:
"##########################
" https://github.com/tpope/vim-fugitive
"Plug 'tpope/vim-fugitive'

"##########################
" plug vim-emoji-ab :
"##########################
" https://github.com/acmpo6ou/vim-emoji-ab
"Plug "acmpo6ou/vim-emoji-ab"
"inoremap <C-Space> C-x C-o
inoremap <C-@> <C-x><C-o>

"##########################
" vim-markdown-preview:
"##########################
" https://github.com/JamshedVesuna/vim-markdown-preview
"Plug 'JamshedVesuna/vim-markdown-preview'
" Render markdown files in local

" Change preview hotkey to <leader>mm
let vim_markdown_preview_hotkey='<leader>mm'

" Use qutebrowser as default browser
let vim_markdown_preview_browser='qutebrowser'

" Additionnal hotkey to run grip
nmap <leader>M :w!<cr>:!grip $(echo %) --export --no-inline ~/.vim/tmp/grip.preview.html<cr>:!qutebrowser ~/.vim/tmp/grip.preview.html<cr><cr>


" preview_toggle
" 0 (default): Preview with hotkey mapping
" 2 : Preview on buffer write
let vim_markdown_preview_toggle=0

" Preview with Github rendering tool (grip)
" 0 (default): Don't use Github
" 1 : Render with GitHub flavoured markdown
"let vim_markdown_preview_github=1

"##########################
" openssl:
"##########################
" https://github.com/
"Plug 'openssl.vim'

"##########################
" Exuberant-ctags:
"##########################
" https://github.com/b4n/ctags or debian package for Sid is SVN based

""""""""""""""""""""""""""""""""""""""""""""""""
" Paramétrage du plugin Taglist
"http://ngressier.developpez.com/articles/vim/vim-plugin-taglist/#LD
"Indispensable pour la deuxième status line définie dans ce fichier
"Si première utilisation, faire un vim dans le dossier ~/.vim/doc/ et faire
":helptags" pour activer le plugin
""""""""""""""""""""""""""""""""""""""""""""""""
"Mappage de l'ouverture et fermeture de la fenêtre des tags avec la touche F4
"nnoremap <silent> <F4> :TlistToggle<CR>
""map <F4> :TlistToogle<CR>
"let Tlist_WinWidth = 50
"let Tlist_Process_File_Always = 1 " activation permanente du plugin pour la barre de statut
"let Tlist_Exit_OnlyWindow = 1  " vim se ferme si il reste uniquement la fenêtre des tags
"let Tlist_Use_Right_Window = 1   " affiche les tags sur le côté droit de l'écran


" ╔═╗╦  ╦ ╦╔═╗
" ╠═╝║  ║ ║║ ╦
" ╩  ╩═╝╚═╝╚═╝ Gestionnaire de plugins vim
" Installation automatique de plug :
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" « :source ~/.vimrc » puis « :PlugInstall ». q pour quitter.
" Commandes : https://github.com/junegunn/vim-plug#commands
call plug#begin('~/.vim/plugged')

	" Colorschemes
	Plug 'dracula/vim'
	Plug 'tomasiser/vim-code-dark'
	Plug 'rakr/vim-one'

	" fzf integration
	Plug 'junegunn/fzf'

	" Display a tree of directories
	Plug 'scrooloose/nerdtree'

	" Super easy commenting, toggle comments, …
	Plug 'scrooloose/nerdcommenter'

	" <Tab> everything
	Plug 'ervandew/supertab'

	" Awesome syntax checker
	" REQUIREMENTS: See :h syntastic-intro
	Plug 'scrooloose/syntastic'
	" For ansible
	Plug 'pearofducks/ansible-vim'

	" Advanced auto-completion system
	Plug 'Shougo/neocomplcache'

	" Markdown syntax
	Plug 'plasticboy/vim-markdown'

	" Tmux conffile syntax
	Plug 'ericpruitt/tmux.vim'
	autocmd BufNewFile,BufRead {.,}tmux*.conf* setfiletype tmux

	" Markdown preview
	Plug 'JamshedVesuna/vim-markdown-preview'

	" A pretty statusline, bufferline integration
	Plug 'itchyny/lightline.vim'
	Plug 'bling/vim-bufferline'
	"Plug 'bling/vim-airline'
	"Bundle 'edkolev/tmuxline.vim'

	" Give viual aid to navigate marks
	Plug 'jacquesbh/vim-showmarks'

	" Furry finder (files, mru, …)
	Plug 'kien/ctrlp.vim'

	" Undo history visualizer
	Plug 'mbbill/undotree'

	" Autoclose (, ", …
	Plug 'Townk/vim-autoclose'

	" Functions, class data …
	" REQUIREMENTS: (exuberant)-ctags
	Plug 'majutsushi/tagbar'

	" Git wrapper inside Vim
	Plug 'tpope/vim-fugitive'

	" Emoji abbreviations and completion
	Plug 'acmpo6ou/vim-emoji-ab'
	" Default type
	"au FileType html,markdown,md,text,mail,gitcommit,vim
	" Try to enable emoji-ab plugin for all files
	au FileType *
    \ runtime plugged/vim-emoji-ab/macros/emoji-ab.vim

call plug#end()


"""""""""""""""""""""""""""""""""""""""""""""""""""
" Dictionnaire français
"""""""""""""""""""""""""""""""""""""""""""""""""""
set dictionary+=/usr/share/dict/french
"Liste des propositions avec CTRL-X_CTRL-K ou <leader>xk
imap <leader>xk 

"""""""""""""""""""""""""""""""""""""""""""""""""""
" Correction orthographique
"-------------En mode INSERTION :------------------
"Liste des propositions :         CTRL-X_s
"-------------En mode COMMANDE :-------------------
"Liste des propositions :         z=
"Prochain mot mal orthographié :  ]s
"Précédent mot mal orthographié : [s
"Ajouter un mot au dictionnaire: zg
"""""""""""""""""""""""""""""""""""""""""""""""""""
"if has("spell")
    "" La commande z= affiche 10 suggestions. En mode insertion: CRTL-X_s
    "set spellsuggest=10
    "" On règle les touches d'activation manuelle de la correction orthographique
    "noremap <leader>sf :setlocal spell spelllang=fr <CR>
    "noremap <leader>se :setlocal spell spelllang=en <CR>
    "noremap <leader>sn :setlocal nospell <CR>
    "" On active automatiquement le mode spell pour les fichiers texte et LaTeX
    "autocmd BufEnter *.txt,*.tex set spell
    "" On applique le dictionnaire français pour tous les types de fichiers
    "autocmd BufEnter * set spelllang=fr
"endif
"if !filewritable ($HOME."/.vim/spell") " Si le répertoire n'existe pas
  "call mkdir($HOME."/.vim/spell", "p") " Création du répertoire de sauvegarde
"endif

" Choix de la langue de l'aide
set helplang=fr

" Liste des abréviations textes:
iabbrev cad c'est-à-dire
iabbrev svp s'il-vous-plaît
iabbrev stp s'il-te-plaît
iabbrev pcq parce que

" hand emoji {{{
iabbrev :ok: 👌
iabbrev :ok_hand: 👌
iabbrev :+1: 👍
iabbrev :+: 👍
iabbrev :-1: 👎
iabbrev :-: 👎
iabbrev :thumbup: 👍
iabbrev :thumbdown: 👎
iabbrev :victoire: ✌️
iabbrev :victory: ✌️
iabbrev :v: ✌️
iabbrev :V: ✌️
"
" }}}
" face emojy {{{
iabbrev :D: 😀
iabbrev :grin: 😀
iabbrev :grinning: 😀
iabbrev -__- 😑
iabbrev :demon_happy: 😈
iabbrev :devil_happy: 😈
iabbrev :happy_demon: 😈
iabbrev :happy_devil: 😈
iabbrev :face_palm: 🤦
iabbrev :facepalm: 🤦
iabbrev :happy: 😊
iabbrev :happy_face: 😊
iabbrev :heart_eye: 😍
iabbrev :heart_eyes: 😍
iabbrev :eyes_heart: 😍
iabbrev :eye_heart: 😍
iabbrev :joy: 😂
iabbrev :kiss_heart: 😘
iabbrev :kissing_heart: 😘
iabbrev :heart_kiss: 😘
iabbrev :kiss: 😙
iabbrev :laugh: 😆
iabbrev :laughing: 😆
iabbrev :lol: 😆
iabbrev :mask: 😷
iabbrev :relieved: 😌
iabbrev :relieve: 😌
iabbrev :rolling: 🤣
iabbrev :sad_relieved: 😥
iabbrev :sad_relieve: 😥
iabbrev :scream: 😱
iabbrev :smile: 🙂
iabbrev :skull: 🕱
iabbrev :pensive: 😔
iabbrev :smirk: 😏
iabbrev :amused: 😏
iabbrev :unamused: 😒
iabbrev :rage: 😡
iabbrev :sob: 😭
iabbrev :sunglass: 😎
iabbrev :sunglasses: 😎
iabbrev :sweat_smile: 😅
iabbrev :sweat_cold: 😓
iabbrev :cold_sweat: 😓
iabbrev :ss: 😅
iabbrev :tired: 😫
iabbrev :tongue: 😛
iabbrev :sad: 😟
iabbrev :worry: 😟
iabbrev :worried: 😟
iabbrev :stuck_out_tongue: 😛
iabbrev :vomi: 🤮
iabbrev :vomiting: 🤮
iabbrev :vomiting_face: 🤮
iabbrev :face_vomiting: 🤮
iabbrev :face_vomi: 🤮
iabbrev :wink: 😉
iabbrev :yum: 😋
iabbrev :yummy: 😋

" }}}
" Symbol emoji {{{
iabbrev :check: ✔
iabbrev :error: ✖
iabbrev :heart: ♥️

" }}}
" Text emoticon {{{
iabbrev :cheerleader: ✽-(ˆ▽ˆ)/✽ ✽\(ˆ▽ˆ)-✽
iabbrev :cheerleaders: ✽-(ˆ▽ˆ)/✽ ✽\(ˆ▽ˆ)-✽
iabbrev :cheer: ✽-(ˆ▽ˆ)/✽ ✽\(ˆ▽ˆ)-✽
iabbrev :piu: ヽ(°◇° )ノ
iabbrev :rah: (╯°□°)╯
iabbrev :shocked: ⊙_⊙
iabbrev :sshrug: ¯\_(ツ)_/¯
iabbrev :teddy: ʕ•ᴥ•ʔ
iabbrev :teddy_bear: ʕ•ᴥ•ʔ
iabbrev :bear: ʕ•ᴥ•ʔ

" }}}

"""""""""""""""""""""""""""""""""""""""""""""""""""
" Modification de la barre de statut
"""""""""""""""""""""""""""""""""""""""""""""""""""
" Pas besoin de plugin
"set laststatus=2 " Affiche la barre de statut quoi qu'il en soit (0 pour la masquer, 1 pour ne l'afficher que si l'écran est divise)
"if has("statusline")
    "set statusline=
    "set statusline+=%<
    "set statusline+=[%02n%H%M%R%W]                 " Numéro du buffer (2 digit), and flags
    "set statusline+=\ %m                           " modified flag '[+]' if modifiable
    "set statusline+=%f                             " Nom du fichier
    "set statusline+=%r                             " read only flag '[RO]'
    "set statusline+=%h                             " help flag '[Help]'
    ""set statusline+=%1*\ [FORMAT=%{&ff}]%0*        " Format du fichier
    "set statusline+=\ [FORMAT=%{&ff}]              " Format du fichier
    "set statusline+=\ [TYPE=%Y]                    " Type de fichier
    "set statusline+=\ [ENC=%{&fileencoding}]      " Encodage du fichier
    "set statusline+=\ [POS=%04l,%03v]              " Position dans le fichier ligne/colonne
    "set statusline+=%=                             " seperate between right- and left-aligned
    "set statusline+=\ [%p%%]                         " Position dans le fichier en %
    "set statusline+=\ [%l/%L]                     " Nombre de ligne dans le fichier
    ""set statusline+=%1*%y%*%*                      " file type
    "set statusline+=%{&hlsearch?'+':'-'}          " Résultat de recherche surligné (+: y; -: n)
    "set statusline+=%{&paste?'=':'\ '}
    "set statusline+=%{&wrap?'<':'>'}

"elseif has("cmdline_info")
    "set ruler " Affiche la position du curseur en bas a gauche de l'écran
"endif


"""""""""""""""""""""""""""""""""""""""""""""""""""
" Création des répertoires
"""""""""""""""""""""""""""""""""""""""""""""""""""
"################################
" Centralisation des backups :
"################################
if !filewritable ($HOME."/.vim/backup") " Si le répertoire n'existe pas
  call mkdir($HOME."/.vim/backup", "p") " Création du répertoire de sauvegarde
endif
" On définit le répertoire de sauvegarde
set backupdir=$HOME/.vim/backup

" On active le comportement précédemment décrit
set backup

"################################
" Undo persistant :
"################################
" !!! Attention à la taille des fichiers de sauvegarde !!!
if has("persistent_undo")
  if !filewritable ($HOME."/.vim/undodir") " Si le répertoire n'existe pas
    call mkdir($HOME."/.vim/undodir", "p") " Création du répertoire de sauvegarde
  endif
  " On définit le répertoire de sauvegarde
  set undodir=$HOME/.vim/undodir " répertoire où seront stockés les modifications
  set undofile                   " activation du undo persistant
  set undolevels=100             " nombre maximum de changements sauvegardés
  set undoreload=100             " nombre maximum de lignes sauvegardées
endif

"################################
" Répertoire de chargement automatique des scripts
"################################
if !filewritable ($HOME."/.vim/autoload") " Si le répertoire n'existe pas
  call mkdir($HOME."/.vim/autoload", "p") " Création du répertoire de chargement automatique
endif

"################################
" Répertoire pour les fichiers temporaires
"################################
" Placer les fichiers .swp dans un autre répertoire
if !filewritable ($HOME."/.vim/tmp") " Si le répertoire n'existe pas
  call mkdir($HOME."/.vim/tmp", "p") " Création du répertoire temporaire
endif
set directory=$HOME/.vim/tmp

"""""""""""""""""""""""""""""""""""""""""""""""""""
" Gestion des templates
"""""""""""""""""""""""""""""""""""""""""""""""""""
" Récupérer le nom du fichier
" :!echo % (renvoie /Documents/fichier.tex )
" :!echo %:r (renvoie /Documents/fichier )
" :!echo %:e (renvoie tex )
" :!echo %:t (renvoie fichier.tex )
" :!echo %:p (renvoie /home/limax/Documents/fichier.tex )
" :!echo %:t (renvoie fichier.tex )
" :!echo %:h (renvoie Documents)

if has("autocmd")
  augroup templates
    autocmd!
    autocmd BufNewFile *.html,*.htm call Template('html')
    autocmd BufNewFile *.py call Template('py')
    autocmd BufNewFile *.sh,*.bat call Template('sh')
    autocmd BufNewFile *.c,*.cpp,*.sc,*.h call Template('c')
    autocmd BufNewFile *.spr call Template('spr')
    autocmd BufNewFile *.rec call Perso_recette('rec')
    autocmd BufNewFile *.tex call Template('tex')
    autocmd BufNewFile *.pp call Template('pp')
  augroup END

  function! Template(type)
    execute "0r ~/.vim/templates/skeleton.".a:type
    execute "%s/!!DATE!!/".strftime("%Y-%m-%d")."/e"
    execute "normal! 10G$"
  endfunction

  function! Perso_recette(type)
    execute "0r ~/.vim/templates/recette.tex"
    execute "%s/!!FICHIER!!/".expand("%:t")."/e"
    execute "%s/!!DATE!!/".strftime("%d-%m-%Y")."/e"
    execute "normal! 10G$"
  endfunction
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""
" Actions Automatiques
"""""""""""""""""""""""""""""""""""""""""""""""""""

" Fix file permissions {{{
"#########################
"""""""""""""""""""""""""""""""""""""""""""""""""""
" Set exec permission if the first line of the current file starts with '#!'
" and contains '/bin/'
function! ModeChange()
  if getline(1) =~ "^#!"
    if getline(1) =~ "/bin/"
      silent !chmod a+x <afile>
    endif
    if getline(1) =~ "/usr/bin/"
      silent !chmod a+x <afile>
    endif
  endif
endfunction

au BufWritePost * call ModeChange()

" }}}

"################################
" Whitespaces and extra character
"################################
" Display '⏎' to mark an end of line
" Display '␠' to mark extra white space at the end of a line
" Display '⎵' to mark nbsp (fixed spaces,…)
set list
"set listchars=eol:⏎,nbsp:⎵,tab:▸·,extends:>,precedes:<,trail:␠
set listchars=nbsp:⎵,tab:▸·,extends:>,precedes:<,trail:␠

" Manage ExtraWhiteSpace {{{
" Display ExtraWhiteSpace (spaces at the End Of the Line) in gray
highlight ExtraWhitespace ctermbg=darkgray guibg=lightred
match ExtraWhitespace /\s\+$/

" Function to clean it
function! CleanTrailingWhitespace()
" https://stackoverflow.com/questions/6496778/vim-run-autocmd-on-all-filetypes-except
	" Specific sed command for mail filetype
	if &ft =~ 'mail'
		" Want to keep email's signature (begin with "--" + one extra whitespace).
		" https://stackoverflow.com/questions/45294980/in-sed-how-to-do-a-pattern-substitution-only-when-another-pattern-doesnt-exist
		:g!/^--/s/\s\+$//g
		return
	endif
	" Default sed command on all other filetypes
	%s/\s\+$//e
endfunction

" Call function before saving
autocmd BufWritePre * call CleanTrailingWhitespace()
" }}}

" Auto-remove extra ⏎ before writing the file
autocmd BufWritePre * :%s/⏎\+$//e


" Search for 2 whitespaces and highlight it (red and underline)
set hls
let g:HLSpace = 1
function! ToggleSpaceUnderscoring()
	if g:HLSpace
		highlight Search cterm=underline gui=underline ctermbg=red ctermfg=none
		let @/ = "  "
	else
		highlight clear
		silent colorscheme "".g:HLColorScheme
		let @/ = ""
	endif
	let g:HLSpace = !g:HLSpace
endfunction

nmap <silent> <F3> <Esc>:call ToggleSpaceUnderscoring()<CR>

"################################
" Configuration BÉPO
"################################
" Si la disposition bépo est détectée, charger automatiquement le fichier
if !empty(system("setxkbmap -print|grep bepo"))
	source ~/.vim/vimrc.bepo
endif
" Chargement manuel pour les machines ne disposant pas de setxkbmap (serveurs)
map <leader>é :source ~/.vim/vimrc.bepo<cr>


"################################
" Conserver emplacement curseur
"################################
set viminfo='10,\"100,:20,%,n~/.viminfo
au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif

" Différences depuis le debut de l'edition
"if !exists(":DiffOrig")
" La commande suivante permet de comparer le fichier avec son ancien etat
" (au moment de l'ouverture dans Vim).
"command DiffOrig vertical new | set buftype=nofile | r # | 0d_ |
"diffthis
"\| wincmd p | diffthis
" Mapping de la commande precedente
"noremap <Leader>ch <Esc> :DiffOrig<CR>
"endif


"################################
" Recharger vimrc après modif
"################################
if has("autocmd")
  autocmd! bufwritepost vimrc source ~/.vim/vimrc
endif


""""""""""""""""""""""""""""""""""""""""""""""""
" Chargement des types de fichiers
""""""""""""""""""""""""""""""""""""""""""""""""
autocmd BufEnter *.txt set filetype=text



"""""""""""""""""""""""""""""""""""""""""""""""""""
" Actions Manuelles
"""""""""""""""""""""""""""""""""""""""""""""""""""

" Clipboard {{{
"################################
" Toggle paste (to avoid extra indent when pasting)
set pastetoggle=<leader>p

" Copy
"" In visual, selected text is send to PRIMARY clipboard
set clipboard=autoselect
"" Send selected text (with visual mode) to clipboard with xclip
vmap <C-c> y:call system("xclip -i -selection clipboard", getreg("\""))<CR>:call system("xclip -i", getreg("\""))<CR>

" Paste
"" Paste clipboard from normal mode
" <C-v> is used for visual block so avoid to map it
noremap <leader>P :call setreg("\"",system("xclip -o -selection clipboard"))<CR>p
" }}}

"################################
" Lancer un navigateur internet
"################################
""" Firefox
" « v<leader>fd » cherche la définition du mot courant dans le TLFI
vmap <leader>fd :<C-U>!firefox "http://www.cnrtl.fr/lexicographie/<cword>" >& /dev/null<CR><CR>
" « v<leader>fs » cherche les synonymes du mot courant dans le TLFI
vmap <leader>fs :<C-U>!firefox "http://www.cnrtl.fr/synonymie/<cword>" >& /dev/null<CR><CR>
" « v<leader>fg » comme ci-dessus mais pour google
vmap <leader>fg :<C-U>!firefox "http://www.google.fr/search?hl=fr&q=<cword>&btnG=Recherche+Google&meta=" >& /dev/null<CR><CR>
"« v<leader>fw » comme ci-dessus mais pour wikipedia
vmap <leader>fw :<C-U>!firefox "http://fr.wikipedia.org/wiki/<cword>" >& /dev/null<CR><CR>
" « v<leader>fc » comme ci-dessus mais pour le conjugueur
vmap <leader>fc :<C-U>!firefox "http://www.leconjugueur.com/php5/index.php?v=<cword>" >& /dev/null<CR><CR>
" « v<leader>fo » ouvre l’url sur laquelle on se trouve dans firefox
vmap <leader>fo :<C-U>!dwb "<cfile>" >& /dev/null &<CR><CR>

""" W3M
" « v<leader>d » cherche la définition du mot courant dans le TLFI
vmap <leader>d :<C-U>!w3m "http://www.cnrtl.fr/lexicographie/<cword>" <CR>
" « v<leader>s » cherche les synonymes du mot courant dans le TLFI
vmap <leader>s :<C-U>!w3m "http://www.cnrtl.fr/synonymie/<cword>" <CR>
" « v<leader>g » comme ci-dessus mais pour google
vmap <leader>g :<C-U>!w3m "http://www.google.fr/search?hl=fr&q=<cword>&btnG=Recherche+Google&meta=" <CR>
"« v<leader>w » comme ci-dessus mais pour wikipedia
vmap <leader>w :<C-U>!w3m "http://fr.wikipedia.org/wiki/<cword>" <CR>
vmap <leader>sw :<C-U>!dig +short txt "<cword>.wp.dg.cx" <CR>
" « v<leader>c » comme ci-dessus mais pour le conjugueur
vmap <leader>c :<C-U>!w3m "http://www.leconjugueur.com/php5/index.php?v=<cword>" <CR>
" « v<leader>o » ouvre l’url sur laquelle on se trouve
vmap <leader>o :<C-U>!w3m "<cfile>" <CR>
" « v<leader>o » lance Wordreference en -> fr
vmap <leader>te :<C-U>!w3m "http://www.wordreference.com/enfr/<cword>" <CR>
" « v<leader>o » lance Wordreference fr -> en
vmap <leader>tf :<C-U>!w3m "http://www.wordreference.com/fren/<cword>" <CR>



" ansible-vim
let g:ansible_attribute_highlight = "bold"
let g:ansible_extra_keywords_highlight = 1
let g:ansible_normal_keywords_highlight = 'Identifier'
let g:ansible_with_keywords_highlight = 'Statement'

